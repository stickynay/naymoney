package com.study.stickyny.naymoney;

import android.content.Context;

import java.util.List;

/**
 * Created by NN on 2016-04-25.
 */

/**
 * 달력과 관련된 년, 월, 요일, 날짜 데이터를 제공합니다.
 */
public interface CalendarProxy {
    String getMonth(); //월 정보
    List<String> getDayAndDate(); //요일과 날짜 정보
}
