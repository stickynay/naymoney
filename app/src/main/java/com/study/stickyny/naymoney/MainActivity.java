package com.study.stickyny.naymoney;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private DateAdapter mDateAdapter;
    private GridView mGridView;
    private List<String> mCalendarList;
    private CalendarManager mCalendarManager;

    private int mStartDayOfWeek = 1; //ex) 일요일:1 / 월요일:2
    int getStartDayOfWeek() {
        return mStartDayOfWeek;
    }
    void setStartDayOfWeek(int startDayOfWeek) { //시작요일 설정 기능 미구현
        mStartDayOfWeek = startDayOfWeek;
    }

    public List<String> getCalendarList() {
        return mCalendarList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCalendarManager = CalendarManager.getInstance();
        mCalendarManager.setContext(this);

        mGridView = (GridView) findViewById(R.id.cal_gridview);
        TextView monthText = (TextView) findViewById(R.id.month_text);
        final ToggleButton startDayToggle = (ToggleButton) findViewById(R.id.startDayToggle);
        startDayToggle.setOnClickListener(new View.OnClickListener() { //아직 미구현
            @Override
            public void onClick(View view) {
                if(startDayToggle.isChecked()) {
                    setStartDayOfWeek(1); //일요일 시작
                } else {
                    setStartDayOfWeek(2); //월요일 시작
                }
            }
        });

        monthText.setText(mCalendarManager.mCalendarProxy.getMonth()); //무시해도 되는 워닝
        showCalendar();
    }

    /**
     * 달력을 출력하기위해 필요한 데이터 설정
     * 역할 : 달력 데이터 연산 / 어댑터에 리스트 연결 / 그리드뷰에 어댑터 연결
     */
    void showCalendar() {
        mCalendarList = new ArrayList<>();
        mCalendarList.addAll(mCalendarManager.mCalendarProxy.getDayAndDate()); //깊은복사
        mDateAdapter = new DateAdapter(this);
        mGridView.setAdapter(mDateAdapter);
    }

    /**
     * 달력을 출력합니다.
     * 토요일은 파랑색, 일요일은 빨강색, 평일은 검정색입니다.
     *
     */
    class DateAdapter extends BaseAdapter {
        LayoutInflater inflater;

        DateAdapter(Context context) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return getCalendarList().size();
        }

        @Override
        public Object getItem(int i) {
            return getCalendarList().get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if(view == null) {
                view = inflater.inflate(R.layout.cal_item, viewGroup, false);
            }
            TextView textView = (TextView) view.findViewById(R.id.Cal_item_TextView);
            textView.setText(getCalendarList().get(i));

            if (getStartDayOfWeek() == 1) { //달력이 일요일부터 시작할 때 컬러 설정
                if (i % 7 == 6) { //토요일
                    textView.setTextColor(Color.BLUE);
                } else if (i % 7 == 0) { //일요일
                    textView.setTextColor(Color.RED);
                } else {
                    textView.setTextColor(Color.BLACK);
                }
            } else if (getStartDayOfWeek() == 2) { //달력이 월요일부터 시작할 때 컬러 설정
                if (i % 7 == 5) { //토요일
                    textView.setTextColor(Color.BLUE);
                } else if (i % 7 == 6) { //일요일
                    textView.setTextColor(Color.RED);
                } else {
                    textView.setTextColor(Color.BLACK);
                }
            }
            return view;
        }
    }
}
