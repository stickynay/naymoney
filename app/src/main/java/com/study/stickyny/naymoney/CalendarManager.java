package com.study.stickyny.naymoney;

import android.content.Context;
import android.content.res.Resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by NN on 2016-04-25.
 */
public class CalendarManager {
    private static CalendarManager sCalendarManager;
    private List<String> mCalendarList;
    private Context mContext;

    public static CalendarManager getInstance() {
        if(sCalendarManager == null) {
            return new CalendarManager();
        } else {
            return sCalendarManager;
        }
    }
    public void setContext(Context context) {
        mContext = context;
    }
    private CalendarManager() {
        mCalendarList = new ArrayList<>();
    }

    private void addDayOfWeekText(List<String> list) { //요일 데이터 추가하기
        list.addAll(
                Arrays.asList(
                        mContext.getString(R.string.sunday), //@Resources.getSystem().getString : string.xml은 어플리케이션 레벨 리소스이므로 NotFoundException 에러 발생
                        mContext.getString(R.string.monday), //@액티비티가 아닌 클래스에서 static 메소드를 사용하려면, context가 반드시 필요하다. 왜?
                        mContext.getString(R.string.tuesday),
                        mContext.getString(R.string.wednesday),
                        mContext.getString(R.string.thursday),
                        mContext.getString(R.string.friday),
                        mContext.getString(R.string.saturday)
                )
        ); //더 좋은 방법이 없을까
    }
    private void addEmptyString(Calendar calendar, List<String> list) { //이전달 영역을 공백으로 채우기
        Calendar newCalendar = Calendar.getInstance();
        newCalendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);
        int firstDay = newCalendar.get(Calendar.DAY_OF_WEEK); //현재 달 1일의 요일 알아내기
        if(firstDay != Calendar.SUNDAY) {
            for(int i=1; i<firstDay; i++) {
                list.add("");
            }
        }
    }
    private void addDay(Calendar calendar, List<String> list) { //현재달의 시작일부터 말일까지 추가하기
        for(int i=0; i<calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            list.add(Integer.toString(i+1));
        }
    }
    private List<String> setCalendarDate() {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH) + 1;
        calendar.set(Calendar.MONTH, month-1);

        //@아래 세 메서드가 순서대로 실행되어야하는데 순서가 보장될까?
        addDayOfWeekText(mCalendarList);
        addEmptyString(calendar, mCalendarList);
        addDay(calendar, mCalendarList);

        return mCalendarList;
    }


    public CalendarProxy mCalendarProxy = new CalendarProxy() {
        @Override
        public String getMonth() {
            Calendar calendar = Calendar.getInstance();
            return mContext.getString(R.string.month_text, calendar.get(Calendar.MONTH)+1);
        }
        @Override
        public List<String> getDayAndDate() {
            return setCalendarDate();
        }
    };
}
